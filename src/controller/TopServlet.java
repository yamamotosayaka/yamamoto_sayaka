package controller;


import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import beans.UserComment;
import beans.UserMessage;
import service.CommentService;
import service.MessageService;


@WebServlet(urlPatterns = { "/index.jsp" })
public class TopServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {



    	/*日付絞り込み*/
        String startDate = request.getParameter("startDate");
        String endDate = request.getParameter("endDate");
        String category = request.getParameter("category");



        if(StringUtils.isBlank(startDate)){
            startDate = "2018-01-01";
        }
        if(StringUtils.isBlank(endDate)){
            Date d = new Date();
            SimpleDateFormat d1 = new SimpleDateFormat("yyyy-MM-dd");
            endDate = d1.format(d);
        }
        	endDate = endDate + " 23:59;59";

       /* if(StringUtils.isBlank(category)){

        }*/

        List<UserMessage> messages = new MessageService().getMessage(startDate, endDate, category);

        request.setAttribute("messages", messages);

        List<UserComment> comments = new CommentService().getComment();
        request.setAttribute("comments", comments);

        request.getRequestDispatcher("/top.jsp").forward(request, response);
    }

/*    カテゴリー検索
    String startDate = request.getParameter("startDate");
    String endDate = request.getParameter("endDate");
    String category = request.getParameter("category");

*/
/*
    if(StringUtils.isBlank(startDate)){
        startDate = "2018-01-01";
    }
    if(StringUtils.isBlank(endDate)){
        Date d = new Date();
        SimpleDateFormat d1 = new SimpleDateFormat("yyyy-MM-dd");
        endDate = d1.format(d);
    }
    endDate = endDate + " 23:59;59";
//    if(StringUtils.isEmpty(category)){
//    	category =
//    }

    List<UserMessage> messages = new MessageService().getMessage(startDate, endDate, category);

    request.setAttribute("messages", messages);

    List<UserComment> comments = new CommentService().getComment();
    request.setAttribute("comments", comments);

    request.getRequestDispatcher("/top.jsp").forward(request, response);
}*/

}