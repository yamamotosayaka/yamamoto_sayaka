package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Branch;
import beans.Department;
import beans.User;
import service.BranchService;
import service.DepartmentService;
import service.UserService;


@WebServlet(urlPatterns = { "/editing" })
public class EditingServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;


	/*編集画面にそのユーザーの情報が入った状態で表示されるようにする*/
	/*既存のユーザーを誰でも編集できるようにする*/
	 @Override
	    protected void doGet(HttpServletRequest request,
	            HttpServletResponse response) throws IOException, ServletException {

		 String editUser_id =request.getParameter("id");
		 HttpSession session1 = request.getSession();

	        if(!editUser_id.matches("^[0-9]{1,}+$") || editUser_id == null){
	        	 List<String> messages = new ArrayList<String>();
	             messages.add("不正なアクセスです");
	             session1.setAttribute("errorMessages", messages);
	             response.sendRedirect("./management");
	             return;

			 }else{
		        User editUser = new UserService().getUser(Integer.parseInt(request.getParameter("id")));
		        if(editUser == null){
		        	 List<String> messages = new ArrayList<String>();
		             messages.add("不正なアクセスです");
		             session1.setAttribute("errorMessages", messages);
		             response.sendRedirect("./management");
		             return;

		        }

		        request.setAttribute("editUser", editUser);


		        HttpSession session = request.getSession();
		        /*支店セレクトボックス*/
		        List<Branch> branchList = new BranchService().branch();
		        session.setAttribute("branchList", branchList);

		        /*役職セレクトボックス*/
		        List<Department> departmentList = new DepartmentService().department();
		        session.setAttribute("departmentList", departmentList);

		        request.getRequestDispatcher("editing.jsp").forward(request, response);

			 }
	    }




	 @Override
	    protected void doPost(HttpServletRequest request,
	            HttpServletResponse response) throws IOException, ServletException {


	        List<String> messages = new ArrayList<String>();
	        HttpSession session = request.getSession();
	        User editUser = getEditUser(request);
	        String password = request.getParameter("password");


	        if (isValid(request, messages) == true) {
	           new UserService().update(editUser, password);
	            response.sendRedirect("./management");
	        } else{
	            session.setAttribute("errorMessages", messages);
	            request.setAttribute("editUser", editUser);
	            request.getRequestDispatcher("editing.jsp").forward(request, response);
	        }
	 	}



	     private User getEditUser(HttpServletRequest request){

	            User editingUser = new User();
	            String password = request.getParameter("password");

	            editingUser.setId(Integer.parseInt(request.getParameter("id")));
	            editingUser.setName(request.getParameter("name"));
	            editingUser.setAccount(request.getParameter("account"));
	            if(!StringUtils.isEmpty(password)){
	            editingUser.setPassword(request.getParameter("password"));
	            }
	            editingUser.setBranch_id(Integer.parseInt(request.getParameter("branch_id")));
	            editingUser.setDepartment_id(Integer.parseInt(request.getParameter("department_id")));
	            return editingUser;

	    }

	  	private boolean isValid(HttpServletRequest request, List<String> messages) {
	  		String name = request.getParameter("name");
	  		String account = request.getParameter("account");
	  		String password = request.getParameter("password");
	  		String check_password = request.getParameter("check_password");
	  		String branch_id = request.getParameter("branch_id");
	  		String department_id = request.getParameter("department_id");
	  		User editUser = new UserService().getUser(Integer.parseInt(request.getParameter("id")));
	  		//入力されたログインIDをDBの既存のログインIDから検索した結果
	  		User updateAccount = new UserService().getAccount(account);



	        if (10 < name.length() || StringUtils.isBlank(name)) {
	            messages.add("ユーザー名を10文字以下で入力してください");
	        }
	  		if (20 < account.length() || 6 > account.length() || StringUtils.isBlank(account)) {
	            messages.add("ログインIDを6文字以上、20文字以下で入力してください");
	        } else if
	  		 (!account.matches("^[a-zA-Z0-9]+$")) {
	            messages.add("ログインIDを半角英数字で入力してください");
	        }
	  		/*①検索結果がnullだったらDBに存在しないログインIDなのでOK
	  		  ②検索結果がnullでなかったらDBに存在するログインIDなのでNG
	  			→編集されているユーザーのID（主キー）が検索結果のユーザーID（主キー）と一致していれば同じユーザーなため、
	  			  ログインアカウントの変更OK*/
	  		if(updateAccount != null && editUser.getId() != updateAccount.getId()){
	  			messages.add("既存のログインIDです");
	  		}
	  		if (!(StringUtils.isEmpty(password) && StringUtils.isBlank(password))){
		  		if ((20 < password.length() || 6 > password.length()) || (StringUtils.isBlank(password))) {
		            messages.add("パスワードを6文字以上、20文字以下で入力してください");
		        } else if
		  			(!password.matches("^[a-zA-Z0-9 -/:-@\\[-\\`\\{-\\~]+$") || (StringUtils.isBlank(password))) {
		            messages.add("パスワードを半角英数字（記号含む）で入力してください");
		        } else if
		  			(!password.equals(check_password)) {
		            messages.add("パスワードと確認用パスワードが一致しません");
		        }
	  		}
/*	  		bra==1 && !(dep1 || 2 )
	  		bra!=1 && (dep1 || 2)*/
	  		if ((department_id.equals("1") || department_id.equals("2")) && !(branch_id.equals("1"))) {
	            messages.add("支店と部署・役職が不正な組み合わせです");
	        }
	  		if (department_id.equals("3") && !(branch_id.equals("2"))) {
	            messages.add("支店と部署・役職が不正な組み合わせです");
	        }
	  		if (department_id.equals("4") && !(branch_id.equals("3"))) {
	            messages.add("支店と部署・役職が不正な組み合わせです");
	        }
	  		if (department_id.equals("5") && !(branch_id.equals("4"))) {
	            messages.add("支店と部署・役職が不正な組み合わせです");
	        }
	  		if (department_id.equals("6") && branch_id.equals("1")) {
	            messages.add("支店と部署・役職が不正な組み合わせです");
	        }
	        if (messages.size() == 0) {
	        	return true;
	        } else {
	        	return false;
	        }
	    }
}


