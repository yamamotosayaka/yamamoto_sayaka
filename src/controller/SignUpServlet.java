package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Branch;
import beans.Department;
import beans.User;
import service.BranchService;
import service.DepartmentService;
import service.UserService;

@WebServlet(urlPatterns = { "/signup" })
public class SignUpServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

//	public static long getSerialversionuid() {
//		return serialVersionUID;
//	}

	//"signup.jsp"をdoGetで画面に表示する
	protected void doGet(HttpServletRequest request,
			 HttpServletResponse response) throws IOException, ServletException {


		 HttpSession session = request.getSession();

		 /*支店セレクトボックス*/
	        List<Branch> branchList = new BranchService().branch();
	        session.setAttribute("branchList", branchList);

	        /*役職セレクトボックス*/
	        List<Department> departmentList = new DepartmentService().department();
	        session.setAttribute("departmentList", departmentList);

	        /*セレクトボックスを取ってきた後にsignup.jspを表示する*/
			 request.getRequestDispatcher("signup.jsp").forward(request, response);
	 }

	protected void doPost(HttpServletRequest request,
	         HttpServletResponse response) throws IOException, ServletException {

		List<String> messages = new ArrayList<String>();
		User editUser = getEditUser(request);

		/*JSPで入力された情報をbeansの箱にセット
		（Accountに、JSPで"account"に入力された情報をセット）*/
		User user = new User();
		user.setAccount(request.getParameter("account"));
		user.setPassword(request.getParameter("password"));
		user.setName(request.getParameter("name"));
		user.setBranch_id(Integer.parseInt(request.getParameter("branch_id")));
		user.setDepartment_id(Integer.parseInt(request.getParameter("department_id")));

		HttpSession session = request.getSession();


		if (isValid(request, messages) == true) {

			new UserService().register(user);

			response.sendRedirect("./management");
		 } else {
	            session.setAttribute("errorMessages", messages);
	            request.setAttribute("editUser", editUser);
	            request.getRequestDispatcher("signup.jsp").forward(request, response);
	     }
	}


	private User getEditUser(HttpServletRequest request){

        User editingUser = new User();

        editingUser.setName(request.getParameter("name"));
        editingUser.setAccount(request.getParameter("account"));
        editingUser.setBranch_id(Integer.parseInt(request.getParameter("branch_id")));
        editingUser.setDepartment_id(Integer.parseInt(request.getParameter("department_id")));
        return editingUser;

	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {
		String account = request.getParameter("account");
        String password = request.getParameter("password");
  		String check_password = request.getParameter("check_password");
        String name = request.getParameter("name");
  		String branch_id = request.getParameter("branch_id");
  		String department_id = request.getParameter("department_id");
  		User loginAccount = new UserService().getAccount(account);
  		System.out.println(loginAccount);

        /*if (StringUtils.isEmpty(account) == true) {
            messages.add("ログインIDを入力してください");
        }*/
        /*if (StringUtils.isEmpty(password) == true) {
            messages.add("パスワードを6文字以上、20文字以下で入力してください");
        }*/
        /*if (StringUtils.isEmpty(name) == true) {
            messages.add("ユーザー名を入力してください");
        }*/
        if (10 < name.length() || StringUtils.isBlank(name)) {
            messages.add("ユーザー名を10文字以下で入力してください");
        }
  		if (20 < account.length() || 6 > account.length() || StringUtils.isBlank(account)) {
            messages.add("ログインIDを6文字以上、20文字以下で入力してください");
        }
  		if (!account.matches("^[a-zA-Z0-9]+$") && !(StringUtils.isBlank(account))) {
            messages.add("ログインIDを半角英数字で入力してください");
        }
  		if (loginAccount != null){
  			messages.add("既存のログインIDです");
  		}
  		if (20 < password.length() || 6 > password.length() || StringUtils.isBlank(password)) {
            messages.add("パスワードを6文字以上、20文字以下で入力してください");
        }
  		if (!account.matches("^[a-zA-Z0-9 -/:-@\\[-\\`\\{-\\~]+$") && !(StringUtils.isBlank(password))) {
            messages.add("パスワードを半角英数字（記号含む）で入力してください");
        }
  		if (!(password.equals(check_password)) && !(StringUtils.isBlank(password)) && !(StringUtils.isBlank(check_password))) {
            messages.add("パスワードと確認用パスワードが一致しません");
        }
        if ((department_id.equals("1") || department_id.equals("2")) && !(branch_id.equals("1"))) {
            messages.add("支店と部署・役職が不正な組み合わせです");
        }
  		if (department_id.equals("3") && !(branch_id.equals("2"))) {
            messages.add("支店と部署・役職が不正な組み合わせです");
        }
  		if (department_id.equals("4") && !(branch_id.equals("3"))) {
            messages.add("支店と部署・役職が不正な組み合わせです");
        }
  		if (department_id.equals("5") && !(branch_id.equals("4"))) {
            messages.add("支店と部署・役職が不正な組み合わせです");
        }
  		if (department_id.equals("6") && branch_id.equals("1")) {
            messages.add("支店と部署・役職が不正な組み合わせです");
        }
        if (messages.size() == 0) {
            return true;
        } else {
		return false;
        }
	}


}
