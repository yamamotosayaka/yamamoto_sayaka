package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import service.CommentService;

@WebServlet(urlPatterns = { "/deleteComment" })
public class DeleteCommentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

		/*top.jspからname="comment_id"の値をとってくる*/
        String comment_id = request.getParameter("comment_id");
        // new サービスのクラス名().呼び出したいメソッド
        new CommentService().deleteComments(Integer.parseInt(comment_id));

        response.sendRedirect("./");
	}


}
