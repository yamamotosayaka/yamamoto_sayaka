package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import service.MessageService;

@WebServlet(urlPatterns = { "/deleteMessage" })
public class DeleteMessageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

    	/*top.jspからname="message_id"の値をとってくる*/
        String message_id = request.getParameter("message_id");
        // new サービスのクラス名().呼び出したいメソッド
        new MessageService().deleteMessages(Integer.parseInt(message_id));

        response.sendRedirect("./");
    }

}

