package controller;


import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Message;
import beans.User;
import service.MessageService;


@WebServlet(urlPatterns = { "/newMessage" })
public class NewMessageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;


	  @Override
	    protected void doGet(HttpServletRequest request,
	            HttpServletResponse response) throws IOException, ServletException {

	        request.getRequestDispatcher("newMessage.jsp").forward(request, response);
	    }


	    @Override
	    protected void doPost(HttpServletRequest request,
	            HttpServletResponse response) throws IOException, ServletException {

	        HttpSession session = request.getSession();
	        Message editMessage = getEditMessage(request);
	        List<String> messages = new ArrayList<String>();

	        if (isValid(request, messages) == true) {

	            User user = (User) session.getAttribute("loginUser");

	            Message message = new Message();
	            /*ノート*/
	            message.setText(request.getParameter("message"));
	            message.setTitle(request.getParameter("title"));
	            message.setCategory(request.getParameter("category"));
	            message.setUser_id(user.getId());

	            new MessageService().register(message);

	            response.sendRedirect("./");
	        } else {
	            session.setAttribute("errorMessages", messages);
	            request.setAttribute("editMessage", editMessage);
	            request.getRequestDispatcher("newMessage.jsp").forward(request, response);
	        }
	    }


	    private Message getEditMessage(HttpServletRequest request){

            Message editMessage = new Message();

/*            editMessage.setId(Integer.parseInt(request.getParameter("id")));*/
            editMessage.setTitle(request.getParameter("title"));
            editMessage.setCategory(request.getParameter("category"));
            editMessage.setText(request.getParameter("message"));
            return editMessage;
	    }


	    private boolean isValid(HttpServletRequest request, List<String> messages) {

	    	String title = request.getParameter("title");
	        String message = request.getParameter("message");
	        String category = request.getParameter("category");


	        if (StringUtils.isBlank(title) == true) {
	            messages.add("件名を30文字以下で入力してください");
	        }
	        if (30 < title.length()) {
	            messages.add("件名を30文字以下で入力してください");
	        }
	        if (StringUtils.isBlank(category) == true) {
	            messages.add("カテゴリーを10文字以下で入力してください");
	        }
	        if (10 < category.length()) {
	            messages.add("カテゴリーを10文字以下で入力してください");
	        }
	        if (StringUtils.isBlank(message) == true) {
	            messages.add("本文を1000文字以下で入力してください");
	        }
	        if (1000 < message.length()) {
	            messages.add("本文を1000文字以下で入力してください");
	        }
	        if (messages.size() == 0) {
	        	return true;
	        } else {
	        	return false;
	        }
	    }
}


