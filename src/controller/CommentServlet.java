package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Comment;
import beans.User;
import service.CommentService;

@WebServlet(urlPatterns = { "/newComment" })
public class CommentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	  @Override
	    protected void doPost(HttpServletRequest request,
	            HttpServletResponse response) throws IOException, ServletException {

	        HttpSession session = request.getSession();
	        Comment editComment = getEditComment(request);

	        List<String> comments = new ArrayList<String>();

	        if (isValid(request, comments) == true) {

	            User user = (User) session.getAttribute("loginUser");

	            Comment comment = new Comment();
	            comment.setMessages_id(Integer.parseInt(request.getParameter("messages_id")));
	            comment.setText(request.getParameter("comment"));
	            comment.setUser_id(user.getId());

	            new CommentService().register(comment);

	            response.sendRedirect("./");
	        } else {
	            session.setAttribute("commentErrorMessages", comments);
	            session.setAttribute("editComment", editComment);
	            session.setAttribute("MessageID", request.getParameter("messages_id"));
	            response.sendRedirect("./?#comment_area_"+ request.getParameter("messages_id"));
//	            request.getRequestDispatcher("top.jsp").forward(request, response);
	        }
	  }


	        private Comment getEditComment(HttpServletRequest request){

	        	Comment editComment = new Comment();

	            editComment.setText(request.getParameter("comment"));
	            editComment.setMessages_id(Integer.parseInt(request.getParameter("messages_id")));
	            return editComment;
		    }


	  	private boolean isValid(HttpServletRequest request, List<String> messages) {
	  		String comment = request.getParameter("comment");

	  		if (StringUtils.isBlank(comment) == true) {
		            messages.add("コメントを500文字以下で入力してください");
		    }
	  		if (500 < comment.length()) {
	            messages.add("コメントを500文字以下で入力してください");
	        }
	        if (messages.size() == 0) {
	        	return true;
	        } else {
	        	return false;
	        }
	    }

}