package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import service.UserService;

@WebServlet(urlPatterns = { "/switching" })
public class SwitchingServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

		/*management.jspからname="user_id"、"is_deleted"の値をとってくる*/
        String user_id = request.getParameter("user_id");
        String is_deleted = request.getParameter("is_deleted");
        // new サービスのクラス名().呼び出したいメソッド
        new UserService().switching(Integer.parseInt(user_id), Integer.parseInt(is_deleted));
     /*   new UserService().update(Integer.parseInt(user_id), Integer.parseInt(is_deleted));*/

        response.sendRedirect("./management");
	}


}
