package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.Management;
import exception.SQLRuntimeException;

public class ManagementDao {

	 public List<Management> getManagements(Connection connection/*, int num*/) {
	        PreparedStatement ps = null;
	        try {
	            StringBuilder sql = new StringBuilder();
	            sql.append("SELECT ");
	            sql.append("users.id as id, ");
	            sql.append("users.name as name, ");
	            sql.append("users.account as account, ");
	            sql.append("branches.name as branch_name, ");
	            sql.append("departments.name as department_name, ");
	            sql.append("users.created_at as created_at, ");
	            sql.append("users.updated_at as updated_at, ");
	            sql.append("users.is_deleted as is_deleted ");
	            sql.append("FROM users ");
	            sql.append("INNER JOIN branches ");
	            sql.append("ON users.branch_id = branches.id ");
	            sql.append("INNER JOIN departments ");
	            sql.append("ON users.department_id = departments.id ");
	            sql.append("ORDER BY branches.id,departments.id,created_at " /*limit" /*+ num */);

/*	            sql.append("department_name, ");
	            sql.append("created_at DESC limit" + num);*/

	            ps = connection.prepareStatement(sql.toString());
	            /*System.out.println(ps);*/

	            ResultSet rs = ps.executeQuery();
	            List<Management> ret = toManagementList(rs);
	            return ret;
	        } catch (SQLException e) {
	            throw new SQLRuntimeException(e);
	        } finally {
	            close(ps);
	        }
	 }


	 private List<Management> toManagementList(ResultSet rs)throws SQLException {

		 List<Management> ret = new ArrayList<Management>();
	        try {
	            while (rs.next()) {
	                int id = rs.getInt("id");
	                String name = rs.getString("name");
	                String account = rs.getString("account");
	                String branch_name = rs.getString("branch_name");
	                String department_name = rs.getString("department_name");
	                Timestamp created_at = rs.getTimestamp("created_at");
	                int is_deleted = rs.getInt("is_deleted");

	                Management management = new Management();
	                management.setId(id);
	                management.setName(name);
	                management.setAccount(account);
	                management.setBranch_name(branch_name);
	                management.setDepartment_name(department_name);
	                management.setCreated_at(created_at);
	                management.setIs_deleted(is_deleted);

	                ret.add(management);
	            }
	            return ret;
	        } finally {
	            close(rs);
	        }
	 }


}
