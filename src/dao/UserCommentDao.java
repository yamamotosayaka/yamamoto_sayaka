package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.UserComment;
import exception.SQLRuntimeException;

public class UserCommentDao {

	 public List<UserComment> getUserComments(Connection connection, int num) {

	        PreparedStatement ps = null;
	        try {
	            StringBuilder sql = new StringBuilder();
	            sql.append("SELECT ");
	            sql.append("comments.id as id, ");
	            sql.append("comments.user_id as user_id, ");
	            sql.append("comments.messages_id as messages_id, ");
	            sql.append("comments.text as text, ");
	            sql.append("users.name as name, ");
	            sql.append("comments.created_at as created_at ");
	            sql.append("FROM comments ");
	            sql.append("INNER JOIN users ");
	            sql.append("ON comments.user_id = users.id ");
	            sql.append("ORDER BY created_at DESC limit " + num);

	            ps = connection.prepareStatement(sql.toString());

	            ResultSet rs = ps.executeQuery();
	            List<UserComment> ret = toUserCommentList(rs);
	            return ret;
	        } catch (SQLException e) {
	            throw new SQLRuntimeException(e);
	        } finally {
	            close(ps);
	        }

	 }

	 private List<UserComment> toUserCommentList(ResultSet rs)throws SQLException {

	        List<UserComment> ret = new ArrayList<UserComment>();
	        try {
	            while (rs.next()) {
	                int id = rs.getInt("id");
	                int user_id = rs.getInt("user_id");
	                int messages_id = rs.getInt("messages_id");
	                String text = rs.getString("text");
	                String name = rs.getString("name");
	                Timestamp created_at = rs.getTimestamp("created_at");

	                UserComment comment = new UserComment();
	                comment.setId(id);
	                comment.setUser_id(user_id);
	                comment.setMessages_id(messages_id);
	                comment.setText(text);
	                comment.setName(name);
	                comment.setCreated_at(created_at);

	                ret.add(comment);
	            }
	            return ret;
	        } finally {
	            close(rs);
	        }
	 }


	 /*コメント削除*/
	 public void comment_id(Connection connection, int id) {

	        PreparedStatement ps = null;
	        try {
	            StringBuilder sql = new StringBuilder();
	            sql.append("DELETE FROM comments ");
	            sql.append("WHERE id = ?");

	            ps = connection.prepareStatement(sql.toString());

	            /*SQLの?にidをセット*/
	            ps.setInt(1, id);

	            ps.executeUpdate();

	        } catch (SQLException e) {
	            throw new SQLRuntimeException(e);
	        } finally {
	            close(ps);
	        }
	 }

}
