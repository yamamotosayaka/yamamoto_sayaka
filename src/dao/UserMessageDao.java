package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import beans.UserMessage;
import exception.SQLRuntimeException;

public class UserMessageDao {

	/*新規投稿*/
    public List<UserMessage> getUserMessages(Connection connection, int num, String startDate, String endDate, String category) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            //messagesテーブルのidをidと名前をつけなおす
            sql.append("messages.id as id, ");
            sql.append("messages.user_id as user_id, ");
            sql.append("users.name as name, ");
            sql.append("messages.title as title, ");
            sql.append("messages.text as text, ");
            sql.append("messages.category as category, ");
            sql.append("messages.created_at as created_at ");
            sql.append("FROM messages ");
            sql.append("INNER JOIN users ");
            //messagesテーブルのidとusersテーブルのidが同じ
            sql.append("ON messages.user_id = users.id ");
            //期間指定
            sql.append("WHERE messages.created_at BETWEEN ? AND ? ");
            if(!(StringUtils.isEmpty(category) || StringUtils.isBlank(category))){
                //カテゴリー検索
                sql.append("AND messages.category LIKE ? ");
            }
            sql.append("ORDER BY created_at DESC limit " + num);

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, startDate);
            ps.setString(2, endDate);
            if(!(StringUtils.isEmpty(category) || StringUtils.isBlank(category))){
                ps.setString(3, "%" + category + "%");
            }

            System.out.println(ps);

            ResultSet rs = ps.executeQuery();
            List<UserMessage> ret = toUserMessageList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<UserMessage> toUserMessageList(ResultSet rs)
            throws SQLException {

        List<UserMessage> ret = new ArrayList<UserMessage>();
        try {
            while (rs.next()) {
                int id = rs.getInt("id");
                int user_id = rs.getInt("user_id");
                String name = rs.getString("name");
                String title = rs.getString("title");
                String text = rs.getString("text");
                String category = rs.getString("category");
                Timestamp created_at = rs.getTimestamp("created_at");

                UserMessage message = new UserMessage();
                message.setId(id);
                message.setUser_id(user_id);
                message.setName(name);
                message.setTitle(title);
                message.setText(text);
                message.setCategory(category);
                message.setCreated_at(created_at);

                ret.add(message);
            }
            return ret;
        } finally {
            close(rs);
        }
    }


    /*投稿削除*/
    public void message_id(Connection connection, int id) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("DELETE FROM messages ");
            sql.append("WHERE id = ?");

            ps = connection.prepareStatement(sql.toString());

            /*SQLの?にidをセット*/
            ps.setInt(1, id);

            ps.executeUpdate();

        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }


    /*日付絞り込み*/
    public  List<UserMessage> searchDate (Connection connection, String startDate, String endDate, int num) {

	    PreparedStatement ps = null;
	    try {

	        StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            //messagesテーブルのidをidと名前をつけなおす
            sql.append("messages.id as id, ");
            sql.append("messages.user_id as user_id, ");
            sql.append("users.name as name, ");
            sql.append("messages.title as title, ");
            sql.append("messages.text as text, ");
            sql.append("messages.category as category, ");
            sql.append("messages.created_at as created_at ");
            sql.append("FROM messages ");
            sql.append("INNER JOIN users ");
            //messagesテーブルのidとusersテーブルのidが同じ
            sql.append("ON messages.user_id = users.id ");
            sql.append("WHERE messages.created_at BETWEEN ? AND ? ");
            sql.append("ORDER BY messages.created_at DESC limit " + num);

	        ps = connection.prepareStatement(sql.toString());
	        ps.setString(1, startDate);
	        ps.setString(2, endDate);

	        ResultSet rs = ps.executeQuery();
	        List<UserMessage> ret = toUserMessageList(rs);
            return ret;
	    } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    /*カテゴリー検索*/
    public List<UserMessage> searchCategory(Connection connection, String category) {

	    PreparedStatement ps = null;
	    try {
	        String sql = "SELECT * FROM messages WHERE category LIKE ?";

	        ps = connection.prepareStatement(sql);
	        ps.setString(1, "%" + category + "%");

	        ResultSet rs = ps.executeQuery();
	        List<UserMessage> ret = toUserMessageList(rs);
	        return ret;
	    } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
}