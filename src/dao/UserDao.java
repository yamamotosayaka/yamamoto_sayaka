package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import beans.User;
import exception.SQLRuntimeException;


public class UserDao {

	/*ユーザー登録機能*/
	public void insert(Connection connection, User user) {
		PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            //INSERT文（テーブルへデータを登録する）
            sql.append("INSERT INTO users ( ");
            sql.append("account");
            sql.append(", password");
            sql.append(", name");
            sql.append(", branch_id");
            sql.append(", department_id");
            sql.append(", created_at");
            sql.append(", updated_at");
            sql.append(", is_deleted");
            sql.append(") VALUES (");
            sql.append("?"); //account
            sql.append(", ?"); //password
            sql.append(", ?"); //name
            sql.append(", ?"); //branch_id
            sql.append(", ?"); //department_id
            sql.append(", CURRENT_TIMESTAMP"); //created_at
            sql.append(", CURRENT_TIMESTAMP"); //updated_at
            sql.append(", ?"); //is_deleted
            sql.append(")");


            //SQLに?の値を入れる
            ps = connection.prepareStatement(sql.toString());


            ps.setString(1, user.getAccount());
            ps.setString(2, user.getPassword());
            ps.setString(3, user.getName());
            ps.setLong(4, user.getBranch_id());
            ps.setLong(5, user.getDepartment_id());
            ps.setLong(6, user.getIs_deleted());
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }


	/*ユーザー登録画面（すでに登録されているIDのバリデーション）
	 * DBからaccountをとってくる*/
	public User getAccount(Connection connection, String account) {

		PreparedStatement ps = null;
	    try {
	        String sql = "SELECT * FROM users WHERE account = ?";

	        ps = connection.prepareStatement(sql);
	        ps.setString(1, account);

	        System.out.println(ps);

	        ResultSet rs = ps.executeQuery();
	        List<User> userList = toUserList(rs);
	        if (userList.isEmpty() == true) {
	            return null;
	        } else if (2 <= userList.size()) {
	            throw new IllegalStateException("2 <= userList.size()");
	        } else {
	            return userList.get(0);
	        }
	    } catch (SQLException e) {
	        throw new SQLRuntimeException(e);
	    } finally {
	        close(ps);
	    }
	}




	/*ログイン*/
	public User getUser(Connection connection, String account,String password) {

	    PreparedStatement ps = null;
	    try {
	        String sql = "SELECT * FROM users WHERE account = ? AND password = ? AND is_deleted = 0";

	        ps = connection.prepareStatement(sql);
	        ps.setString(1, account);
	        ps.setString(2, password);

	        ResultSet rs = ps.executeQuery();
	        List<User> userList = toUserList(rs);
	        if (userList.isEmpty() == true) {
	            return null;
	        } else if (2 <= userList.size()) {
	            throw new IllegalStateException("2 <= userList.size()");
	        } else {
	            return userList.get(0);
	        }
	    } catch (SQLException e) {
	        throw new SQLRuntimeException(e);
	    } finally {
	        close(ps);
	    }
	}

	/*ログイン*/
	public User getUser(Connection connection, int id) {

	    PreparedStatement ps = null;
	    try {
	        String sql = "SELECT * FROM users WHERE id = ?";

	        ps = connection.prepareStatement(sql);
	        ps.setInt(1, id);


	        ResultSet rs = ps.executeQuery();
	        List<User> userList = toUserList(rs);
	        if (userList.isEmpty() == true) {
	            return null;
	        } else if (2 <= userList.size()) {
	            throw new IllegalStateException("2 <= userList.size()");
	        } else {
	            return userList.get(0);
	        }
	    } catch (SQLException e) {
	        throw new SQLRuntimeException(e);
	    } finally {
	        close(ps);
	    }
	}

	 private List<User> toUserList(ResultSet rs) throws SQLException {

	        List<User> ret = new ArrayList<User>();
	        try {
	            while (rs.next()) {
	            	int id = rs.getInt("id");
	                String account = rs.getString("account");
	                String password = rs.getString("password");
	                String name = rs.getString("name");
	                int branch_id = rs.getInt("branch_id");
	                int department_id = rs.getInt("department_id");
	                Timestamp created_at = rs.getTimestamp("created_at");
	                Timestamp updated_at = rs.getTimestamp("updated_at");
	                int is_deleted = rs.getInt("is_deleted");

	                User user = new User();
	                user.setId(id);
	                user.setAccount(account);
	                user.setPassword(password);
	                user.setName(name);
	                user.setBranch_id(branch_id);
	                user.setDepartment_id(department_id);
	                user.setCreated_at(created_at);
	                user.setUpdated_at(updated_at);
	                user.setIs_deleted(is_deleted);

	                ret.add(user);
	            }
	            return ret;
	        } finally {
	            close(rs);
	        }
	 }


	 /*ユーザー編集画面*/
	 public void update(Connection connection, User user, String password) {

	        PreparedStatement ps = null;
	        try {
	            StringBuilder sql = new StringBuilder();
	            sql.append("UPDATE users SET");
	            sql.append(" name = ?");
	            sql.append(",  account = ?");
	            if(!StringUtils.isEmpty(password)){
	            	sql.append(", password = ?");
	            }
	            sql.append(", branch_id = ?");
	            sql.append(", department_id = ?");
	            sql.append(", updated_at = CURRENT_TIMESTAMP");
	            sql.append(" WHERE");
	            sql.append(" id = ?");

	            ps = connection.prepareStatement(sql.toString());

	            ps.setString(1, user.getName());
	            ps.setString(2, user.getAccount());
	            if(!StringUtils.isEmpty(password)){
	            	ps.setString(3, user.getPassword());
	            	ps.setInt(4, user.getBranch_id());
		            ps.setInt(5, user.getDepartment_id());
		            ps.setInt(6, user.getId());
	            }else{
		            ps.setInt(3, user.getBranch_id());
		            ps.setInt(4, user.getDepartment_id());
		            ps.setInt(5, user.getId());
	            }

	            System.out.println(ps.toString());

	            ps.executeUpdate();
	        } catch (SQLException e) {
	            throw new SQLRuntimeException(e);
	        } finally {
	            close(ps);
	        }
	 }


	 /*アカウント停止復活*/
	 public void switching(Connection connection, int id, int is_deleted) {

	        PreparedStatement ps = null;
	        try {
	            StringBuilder sql = new StringBuilder();
	            sql.append("UPDATE users SET");
	            sql.append(" is_deleted = ?");
	            sql.append(" WHERE");
	            sql.append(" id = ?");

	            ps = connection.prepareStatement(sql.toString());

	            ps.setInt(1, is_deleted);
	            ps.setInt(2, id);

	            System.out.println(ps.toString());

	            ps.executeUpdate();
	           /*
	            if (count == 0) {
	                throw new NoRowsUpdatedRuntimeException();
	            }*/
	        } catch (SQLException e) {
	            throw new SQLRuntimeException(e);
	        } finally {
	            close(ps);
	        }
	 }



}

