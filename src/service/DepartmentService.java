package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.Department;
import dao.DpartmentDao;

public class DepartmentService {

	public List<Department> department() {

		  Connection connection = null;
	        try {
	            connection = getConnection();

	            DpartmentDao departmentDao = new DpartmentDao();
	            List<Department> departmentList = departmentDao.getDepartment(connection);

	            commit(connection);
	            return departmentList;
	        } catch (RuntimeException e) {
	            rollback(connection);
	            throw e;
	        } catch (Error e) {
	            rollback(connection);
	            throw e;
	        } finally {
	            close(connection);
	        }
	}
}

