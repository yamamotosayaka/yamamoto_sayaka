package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;

import org.apache.commons.lang.StringUtils;

import beans.User;
import dao.UserDao;
import utils.CipherUtil;

public class UserService {

    public void register(User user) {

        Connection connection = null;
        try {
            connection = getConnection();

            String encPassword = CipherUtil.encrypt(user.getPassword());
            user.setPassword(encPassword);

            UserDao userDao = new UserDao();
            userDao.insert(connection, user);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    public User getUser(int id) {

        Connection connection = null;
        try {
            connection = getConnection();



            UserDao userDao = new UserDao();
            User user = userDao.getUser(connection, id);

            commit(connection);

            return user;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
   }


    /*ユーザー津録画面（すでに登録されているIDのバリデーション）
     * ログインID(account)をDBから検索する*/

    public User getAccount(String account) {

        Connection connection = null;
        try {
            connection = getConnection();



            UserDao userDao = new UserDao();
            User user = userDao.getAccount(connection, account);

            commit(connection);

            return user;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
   }


    /*ユーザー編集*/
    public void update(User user, String password) {

        Connection connection = null;
        try {
            connection = getConnection();

            if(!StringUtils.isBlank(password)){
	            String encPassword = CipherUtil.encrypt(user.getPassword());
	            user.setPassword(encPassword);
            }

            UserDao userDao = new UserDao();
            userDao.update(connection, user, password);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }


    /*アカウント停止復活*/
    public void switching(int id, int is_deleted) {

    	 Connection connection = null;
         try {
             connection = getConnection();

             UserDao userDao = new UserDao();
             userDao.switching(connection, id, is_deleted);

             commit(connection);

         } catch (RuntimeException e) {
             rollback(connection);
             throw e;
         } catch (Error e) {
             rollback(connection);
             throw e;
         } finally {
             close(connection);
         }
     }

}