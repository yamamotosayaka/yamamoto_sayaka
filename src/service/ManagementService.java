package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.Management;
import dao.ManagementDao;

public class ManagementService {



    private static final int LIMIT_NUM = 1000;

    public List<Management> getManagement() {

        Connection connection = null;
        try {
            connection = getConnection();

            ManagementDao managementDao = new ManagementDao();
            List<Management> ret = managementDao.getManagements(connection/*, LIMIT_NUM*/);

            commit(connection);

            return ret;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

}
