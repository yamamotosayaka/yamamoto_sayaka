package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.Message;
import beans.UserMessage;
import dao.MessageDao;
import dao.UserMessageDao;

public class MessageService {

    public void register(Message message) {

        Connection connection = null;
        try {
            connection = getConnection();

            MessageDao messageDao = new MessageDao();
            messageDao.insert(connection, message);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }


    private static final int LIMIT_NUM = 1000;

    public List<UserMessage> getMessage(String startDate, String endDate, String category) {

        Connection connection = null;
        try {
            connection = getConnection();

            UserMessageDao messageDao = new UserMessageDao();
            List<UserMessage> ret = messageDao.getUserMessages(connection, LIMIT_NUM, startDate,  endDate, category);

            commit(connection);

            return ret;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    /*投稿削除（UserMessageDaoにmessage_idを渡す*/
    //public void ここで名前付ける(Daoの中身) {
    public void deleteMessages(int id) {

        Connection connection = null;
        try {
            connection = getConnection();

            UserMessageDao userMessageDao = new UserMessageDao();
            userMessageDao.message_id(connection, id);

            commit(connection);

        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    /*日付絞込み*/
    public  List<UserMessage> search (String startDate, String endDate) {

        Connection connection = null;
        try {
            connection = getConnection();

            UserMessageDao userMessageDao = new UserMessageDao();
            List<UserMessage> ret = userMessageDao.searchDate(connection, startDate, endDate, LIMIT_NUM);

            commit(connection);
            return ret;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }


    /*カテゴリー絞込み*/
    public  List<UserMessage> searchCategory (String category) {

        Connection connection = null;
        try {
            connection = getConnection();

            UserMessageDao userMessageDao = new UserMessageDao();
            List<UserMessage> ret = userMessageDao.searchCategory(connection, category);

            commit(connection);
            return ret;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }


}