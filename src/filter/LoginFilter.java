package filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;

@WebFilter("/*")
public class LoginFilter implements Filter {

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {

		String path = ((HttpServletRequest) request).getServletPath();

		HttpSession session = ((HttpServletRequest) request).getSession();

		 User user = (User) session.getAttribute("loginUser");

		 // セッションがNullかつURLが/loginならば、ログイン画面へ飛ばす(ログインしていなけば、ログイン画面以外のページに飛んだらログイン画面に飛ばされる)
		if (user == null && !"/login".equals(path) && !path.matches(".*css.*")) {
            List<String> messages = new ArrayList<String>();
            messages.add("ログインしてください");
            session.setAttribute("errorMessages", messages);
			((HttpServletResponse) response).sendRedirect("./login");
            return;
		}

		chain.doFilter(request, response); // サーブレットを実行

	}

	@Override
	public void init(FilterConfig config) {
	}

	@Override
	public void destroy() {
	}

}