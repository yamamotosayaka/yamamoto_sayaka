package filter;


import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;


@WebFilter(urlPatterns = { "/management" , "/signup", "/editing" })
public class AuthorityFilter implements Filter {

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {


		HttpSession session = ((HttpServletRequest) request).getSession();

		 User user = (User) session.getAttribute("loginUser");

		 if (user == null) {
				chain.doFilter(request, response); // サーブレットを実行
				return;
		 }

		if (!(user.getBranch_id() == 1 && user.getDepartment_id() == 1)) {
            List<String> messages = new ArrayList<String>();
            messages.add("アクセス権限を持っていません。");
            session.setAttribute("errorMessages", messages);
			((HttpServletResponse) response).sendRedirect("./");
            return;
		}

		chain.doFilter(request, response); // サーブレットを実行

	}

	@Override
	public void init(FilterConfig config) {
	}

	@Override
	public void destroy() {
	}


}
