package beans;

import java.io.Serializable;
import java.util.Date;

public class Comment implements Serializable {
	 private static final long serialVersionUID = 1L;


	 private int id;
	 private int user_id;
	 private int messages_id;
	 private String text;
	 private Date created_at;
	 private Date updated_at;
	 
	 
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getUser_id() {
		return user_id;
	}
	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}
	public int getMessages_id() {
		return messages_id;
	}
	public void setMessages_id(int messages_id) {
		this.messages_id = messages_id;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public Date getCreated_at() {
		return created_at;
	}
	public void setCreated_at(Date created_at) {
		this.created_at = created_at;
	}
	public Date getUpdated_at() {
		return updated_at;
	}
	public void setUpdated_at(Date updated_at) {
		this.updated_at = updated_at;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
