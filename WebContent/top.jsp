<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
    	<link href="./css/style.css" rel="stylesheet" type="text/css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>ホーム画面</title>

        <script>
				function deleteMessageConfirm() {
					return window.confirm("投稿を削除しますか？");
				}
			</script>
			<script>
				function deleteCommentConfirm() {
					return window.confirm("コメントを削除しますか？");
				}
			</script>
    </head>
    <body>
    	<div id="header-fixed">
	    	<div class="top_title">社内掲示板</div>


			    <c:if test="${ empty loginUser }">
			        <a href="login" class="square_btn">ログイン</a>
			        <a href="signup" class="square_btn">登録する</a>
			    </c:if>

		        <!-- <a href="./" class="square_btn">ホーム</a> -->
		        <a href="newMessage" class="square_btn">新規投稿</a>
		        <c:if test="${loginUser.branch_id == 1 && loginUser.department_id == 1 }" >
		       		 <a href="management" class="square_btn">ユーザー管理</a>
		        </c:if>
		        <a href="logout" class="logout_btn">ログアウト</a>
		<br/>
		</div>


	<div class="box">
		<c:if test="${ not empty loginUser }">

		    <!-- 表示の絞込み -->
			<div class="categorySearch">
				 <c:if test="${ not empty errorMessages }">
	                <div class="errorMessages">
	                   <c:forEach items="${errorMessages}" var="message">
	                       <c:out value="${message}" />
	                   </c:forEach>
	                </div>

	            </c:if>

				<!-- 絞込み（カテゴリー、期間指定） -->
	            <form action="./" method="get" class="search_box">
	            	<br/>
	                <p><label for="searchCategry">カテゴリー</label>
		                <input name="category" id="category" value="" size="15"/>
		                	<br/><br/>
		            <label for="date">期間指定</label>
		                <input type="date" name="startDate" id="created_at" value=""/><br/>|<br/>
		                <input type="date" name="endDate" id="created_at" value=""/><br/><br/>


		            <input type="submit" id="search_btn" value="検索" /></p>
	            </form>
			</div>

			<br>

			<!-- 新規投稿表示 -->
			<div class="messagesBox">
			    <c:forEach items="${messages}" var="message">
			    	<c:if test="${startDate }"></c:if>
			            <div class="messageBox">
			    		<!-- <div class="one-message"> -->
			           	<br/>
		                <span  class="title"><c:out value="${message.title}"/></span><br/>
	            		<div class="textBox">
	            		<br/>
					         <span class="messageText">
							    <c:forEach items="${fn:split(message.text,'
							        ')}" var="str1"><c:out value="${str1}" /><br/></c:forEach>
					         </span>
					    <br/>

					         <hr class="vertical-lines"></hr>

					         <p><span class="category">カテゴリー：<c:out value="${message.category}" /></span></p>
				             <p><span  class="messageName">投稿者：<c:out value="${message.name}" /></span></p>
							 <span class="date"><fmt:formatDate value="${message.created_at}" pattern="yyyy/MM/dd HH:mm:ss" /></span>
			            <!-- </div> -->


							<!-- 投稿削除 -->
							<div class="delete">
								<!-- 投稿したユーザーとログインしているユーザーのIDが一致しているかのif -->
						        <c:if test="${message.user_id == loginUser.id }">
								<form action = "deleteMessage" method = "post">
							        		<!-- IDが一致している投稿にのみ削除ボタンを表示する。
							        		hiddenでmessage_idを渡す -->
							                <input type="hidden" name="message_id" value="${message.id}">
							                <input type="submit" value="削除" onClick="return deleteMessageConfirm();" class="del_btn">
							   </form>
							   </c:if>
						    </div>
					     </div>
					    <!--  </div> -->



			            <!-- コメント表示 -->
						<div class="comments">

							    <c:forEach items="${comments}" var="comment">
							    	<c:if test="${message.id == comment.messages_id }">
							    	<div class="one-comment">
<!-- 							            <div class="comment"> -->
						            	<br/>
						                <div class="text"><c:forEach items="${fn:split(comment.text,'
						                ')}" var="str"><c:out value="${str}"></c:out><br/></c:forEach></div>

						                <div class="comment-item">
						                   <c:out value="${comment.name}" />さんからのコメント<br/>
						                   <fmt:formatDate value="${comment.created_at}" pattern="yyyy/MM/dd HH:mm:ss" />

<!-- 							            </div> -->


								            <!-- コメント削除 -->
									            <form action = "deleteComment" method = "post">
													<!-- コメントしたユーザーとログインしているユーザーのIDが一致しているかのif -->
											        <c:if test="${comment.user_id == loginUser.id }">
											        	<!-- IDが一致しているコメントにのみ削除ボタンを表示する。
											        	hiddenでcomment_idを渡す -->
											            <input type="hidden" name="comment_id" value="${comment.id}">
											            <input type="submit" value="削除" onClick="return deleteCommentConfirm();" class="commentdel_btn">
											       		<br/>
											        </c:if>
								        		</form>
						        		</div>
						        		</div>
							        </c:if>
							    </c:forEach>



						<br>

			            <!-- コメント投稿フォーム -->
						<div class="form-area" id="comment_area_${message.id}">
						 <c:if test="${ not empty loginUser }">
				           <%--  <c:if test="${ not empty errorMessages }">
				            	<c:if test="${message.id == comment.messages_id }">
				                <div class="errorMessages">
				                    <ul>
				                        <c:forEach items="${errorMessages}" var="message">
				                            <li><c:out value="${message}" />
				                        </c:forEach>
				                    </ul>
				                </div>
				                <c:remove var="errorMessages" scope="session"/>
				                </c:if>
				            </c:if> --%>

							<br/>
				            <hr class="comment-lines"></hr>
				            <br/>

							<c:if test="${not empty message.id && message.id == MessageID}">
				             <c:if test="${ not empty commentErrorMessages }">

				                <div class="errorMessages">
				                        <c:forEach items="${commentErrorMessages}" var="err_message">
				                            <c:out value="${err_message}" />
				                        </c:forEach>
				                        <c:remove var="commentErrorMessages" scope="session"/>
				                </div>

				                </c:if>
				             </c:if>

						        <form action="newComment" method="post" >
						            <input type="hidden" name="messages_id" value="${message.id}">
						            コメント（500文字以下）<br />
						            <textarea name="comment" cols="100" rows="5" class="tweet-box"><c:if test="${editComment.messages_id == message.id }"><c:out value="${editComment.text}" /></c:if></textarea>
						            <br />
						            <input type="submit" value="投稿" id="comment_btn">
						        </form>
					  	 </c:if>
						</div>
			</div>
						<br/><br/>
					</div>
			    </c:forEach>
			    <c:remove var="errorMessages" scope="session"/>
			    <c:remove var="editComment" scope="session"/>
			</div>
		</c:if>
	</div>
		<div class="copyright"> Copyright(c)Sayaka Yamamoto</div>

    </body>
</html>
