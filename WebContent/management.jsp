<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<link href="./css/style.css" rel="stylesheet" type="text/css">
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>ユーザー管理画面</title>


			<script>
				function stopConfirm() {
					return window.confirm("アカウントを停止しますか？");
				}
			</script>
			<script>
				function startConfirm() {
					return window.confirm("アカウントを復活しますか？");
				}
			</script>

	</head>
	<body>
		<div id="header-fixed">
			<div class="management_title">ユーザー管理</div>

			 <c:if test="${ not empty loginUser }">
			   <a href="./" class="square_btn">ホーム</a>
			   <a href="signup" class="square_btn">新規登録</a>
			   <a href="logout" class="logout_btn"> ログアウト </a>
		 </c:if>
		 </div>

		 <!-- •不正なパラメーター -->
		<c:if test="${ not empty errorMessages }">
             <div class="errorMessages">
                  <c:forEach items="${errorMessages}" var="message">
                      <c:out value="${message}" />
                      <br/>
                  </c:forEach>
             </div>
             <c:remove var="errorMessages" scope="session"/>
        </c:if>
        <br/>

		 	<table border="1">
		 		<tr>
		 			<th>ユーザー名</th><th>ログインID</th><th>支店</th><th>部署・役職</th><th>登録日</th><th>活動状態</th><th>ユーザー編集</th>
				</tr>
				<c:forEach items="${managements}" var="management">
					<tr>
						<td><span class="edit"><c:out value="${management.name}" /></span></td>
						<td><span class="edit"><c:out value="${management.account}" /></span></td>
						<td><span class="edit"><c:out value="${management.branch_name}" /></span></td>
						<td><span class="edit"><c:out value="${management.department_name}" /></span></td>
						<td><span class="edit"><c:out value="${management.created_at}" /></span></td>

							<!-- アカウント停止復活 -->
						<td>
							<c:if test = "${management.id == loginUser.id}">
								<span class="edit">ログイン中</span>
							</c:if>

							<c:if test = "${management.is_deleted  == 0 && management.id != loginUser.id}">
								<span class="edit">活動中</span>
								<form action="switching" method="post">
									<input type="hidden" name="user_id" value=${ management.id } />
									<input type="hidden" name="is_deleted" value="1" />
									<input type="submit" name="stop" value="停止する" id = "stop" onClick="return stopConfirm();" class="stop_btn"/>
								</form>
							</c:if>

							<c:if test = "${management.is_deleted == 1}">
							<span class="edit">停止中</span>
								<form action="switching" method="post">
									<input type="hidden" name="user_id" value=${ management.id } />
									<input type="hidden" name="is_deleted" value="0" />
									<input type="submit" name="revival" value="再開する" id = "stop" onClick="return startConfirm();" class="start_btn"/>
								</form>
							</c:if>
						</td>
						<td><a href="editing?id=${management.id }" class="edit_btn">編集</a></td>
					</tr>
				</c:forEach>
			</table>
		<br/>
		<div class="copyright"> Copyright(c)Sayaka Yamamoto</div>
	</body>
</html>