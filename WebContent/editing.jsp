<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<link href="./css/style.css" rel="stylesheet" type="text/css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>ユーザー編集画面</title>
	</head>
	<body>
		<div id="header-fixed">
			<div class="edit_title">ユーザー編集</div>

				<a href="./" class="square_btn"> ホーム </a>
				<a href="./management" class="square_btn"> ユーザー管理 </a>
				<a href="logout" class="logout_btn"> ログアウト </a>
		</div>


		<div class="errorMessages">
            <c:if test="${ not empty errorMessages }">
              <p class="error_txt">
                <c:forEach items="${errorMessages}" var="message">
                    <c:out value="${message}" />
                    <br/>
                </c:forEach>
              </p>
                <c:remove var="errorMessages" scope="session" />
            </c:if>
        </div>


		<div class="new_box">
			<form action = "" method = "post">
				<!-- hiddenでidを渡してあげる -->
			 	<input name = "id" value = "${editUser.id}" id = "id" type = "hidden"/>
				<span class="name"><label for = "name"> ユーザー名</label></span><br/><span class="name_text">[10文字以下]</span><br />
		   	    <input name = "name" id = "name" value = "${editUser.name}"/><br />

					<br/>

				<span class="name"><label for = "id"> ログインID</label></span><br/><span class="name_text">[半角英数字　6文字以上20文字以下]</span><br />
				<input name = "account" id = "account" value = "${editUser.account}"/><br />

					<br/>

		   	    <span class="name"><label for = "password"> パスワード</label></span><br/><span class="name_text">[半角文字(記号を含む)　6文字以上20文字以下]</span><br />
		   	    <input name = "password" type = "password" id = "password" /><br />

					<br/>

		   	    <span class="name"><label for = "check_password"> パスワード確認 </label></span><br/>
		   	    <input name = "check_password" type="password" id = "check_password" /><br />

				<p><span class="name">支店</span><br />
					<select name = "branch_id" id = "branch" >
						<c:forEach items = "${branchList}" var = "branch">
														<!-- == var.id -->
							<c:if test="${editUser.branch_id == branch.id}">
								<option value = "${branch.id}" selected >${branch.name}</option>
							</c:if>
							<c:if test="${editUser.branch_id != branch.id}">
								<option value = "${branch.id}" >${branch.name}</option>
							</c:if>
						</c:forEach>
					</select>
				</p>

				<p><span class="name">部署・役職</span><br />
					<select name = "department_id" id = "department" >
						<c:forEach items = "${departmentList}" var = "department">
														<!-- == var.id -->
							<c:if test="${editUser.department_id == department.id}">
								<option value = "${department.id}" selected >${department.name}</option>
							</c:if>
							<c:if test="${editUser.department_id != department.id}">
								<option value = "${department.id}" >${department.name}</option>
							</c:if>
						</c:forEach>
					</select>
				</p>


		   	    <input type = "submit" value = "変更" id="new_btn" />

		     </form>
		</div>
		<br/>
		<div class="copyright"> Copyright(c)Sayaka Yamamoto</div>
    </body>
</html>