<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <link href="./css/style.css" rel="stylesheet" type="text/css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>ログイン</title>
    </head>
    <body>
    	<div class="login_title">ログイン</div>
		<br />
	        <!-- <div class="login"> -->
	            <c:if test="${ not empty errorMessages }">
	                <div class="errorMessages">
						<!-- <p class="error_txt"> -->
	                        <c:forEach items="${errorMessages}" var="message">
	                            <c:out value="${message}" />
	                        </c:forEach>
						<!-- </p> -->
	                </div>
	                <c:remove var="errorMessages" scope="session"/>
	            </c:if>

				<div class="login_box">
		            <form action="login" method="post"><br />
		                <span class="login_id"><label for="account">ログインID</label></span><br/>
		                <input type="text" size="16" class="login_form" name="account" id="account" value="${account}"/> <br/>
						<br/>
						<!-- type="password"で伏字に -->
		                <span class="password"><label for="password">パスワード</label></span><br/>
		                <input type="password" size="16" class="login_form" name="password" type="password" id="password" value=""/> <br/>
						<br/>
		                <span class="login_btn"><input type="submit" value="ログイン"  id="login_btn"/></span><br />
		            </form>
	            </div>
	        <!-- </div> -->
        <div class="copyright"><br/> Copyright(c)Sayaka Yamamoto</div>
    </body>
</html>