<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<link href="./css/style.css" rel="stylesheet" type="text/css">
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>新規投稿</title>
	</head>
	<body>
		<div id="header-fixed">
			<div class="new_message_title">新規投稿</div>

				<a href="./" class="square_btn"> ホーム </a>
				 <c:if test="${loginUser.branch_id == 1 && loginUser.department_id == 1 }" >
		       		 <a href="management" class="square_btn">ユーザー管理</a>
		        </c:if>
				<a href="logout" class="logout_btn"> ログアウト </a>
		</div>

		<div class="errorMessages">
            <c:if test="${ not empty errorMessages }">
                <p class="error_txt">
                    <c:forEach items="${errorMessages}" var="message">
                        <c:out value="${message}" />
                         <br/>
                    </c:forEach>
                </p>
              <c:remove var="errorMessages" scope="session"/>
            </c:if>
        </div>


		<div class="new_box">
		  <form action="newMessage" method="post">
		    <span class="name"><label for = "title"> 件名</label></span><br/><span class="name_text">[30文字以下] </span><br />
			 <input name = "title" id = "title" value = "${editMessage.title}" /><br />

				<br/>

			<span class="name"><label for = "category"> カテゴリー</label></span><br/><span class="name_text">[10文字以下]</span><br />
			 <input name="category" id = "category" value = "${editMessage.category}"/><br />

				<br/>

		    <span class="name"><label for = "text">本文</label></span><br/><span class="name_text">[1000文字以下]</span><br />
		     <textarea name="message" cols=10 rows=4 class="tweet-box" ><c:out value="${editMessage.text}" /></textarea>

		     	<br />

		    <input type="submit" value="投稿" id="new_btn">

		 </form>
		</div>
		<br/>
		<div class="copyright"> Copyright(c)Sayaka Yamamoto</div>
	</body>
</html>